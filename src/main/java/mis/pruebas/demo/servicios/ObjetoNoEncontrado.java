package mis.pruebas.demo.servicios;

public class ObjetoNoEncontrado extends RuntimeException {
    public ObjetoNoEncontrado(String message) {
        super(message);
    }
}
