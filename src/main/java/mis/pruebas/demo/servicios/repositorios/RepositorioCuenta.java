package mis.pruebas.demo.servicios.repositorios;

import mis.pruebas.demo.modelos.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCuenta extends MongoRepository<Cuenta, String> {
}
