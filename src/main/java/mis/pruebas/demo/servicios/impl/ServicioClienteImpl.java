package mis.pruebas.demo.servicios.impl;

import mis.pruebas.demo.modelos.Cliente;
import mis.pruebas.demo.modelos.Cuenta;
import mis.pruebas.demo.servicios.ObjetoNoEncontrado;
import mis.pruebas.demo.servicios.ServicioCliente;
import mis.pruebas.demo.servicios.repositorios.RepositorioCliente;
import mis.pruebas.demo.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioClienteImpl implements ServicioCliente {


    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Override
    public void crearCliente(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        final Optional<Cliente> quizasCliente = this.repositorioCliente.findById(documento);
        if (!quizasCliente.isPresent())
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + documento);
        return quizasCliente.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {

        if (!this.repositorioCliente.existsById(cliente.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + cliente.documento);
        this.repositorioCliente.save(cliente);
    }

    @Override
    public void borrarCliente(String documento) {
        this.repositorioCliente.deleteByDocumento(documento);
    }

    @Override
    public Page<Cliente> listaClientes(int pagina, int cantidad) {

        return this.repositorioCliente.findAll(PageRequest.of(pagina, cantidad));
    }

    @Override
    public void emparcharCliente(Cliente parche) {
       /* final Cliente existente = this.clientesMap.get(parche.documento);

        if (parche.edad != existente.edad)
            existente.edad = parche.edad;

        if (parche.nombre != null)
            existente.nombre = parche.nombre;

        if (parche.correo != null)
            existente.correo = parche.correo;

        if (parche.direccion != null)
            existente.direccion = parche.direccion;

        if (parche.telefono != null)
            existente.telefono = parche.telefono;

        if (parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.clientesMap.replace(existente.documento, existente);*/
        throw new UnsupportedOperationException("NO IMPLEMANTADO");
    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = obtenerCliente(documento);
        if (!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numeroCuenta);

        cliente.codigosCuentas.add(numeroCuenta);
        this.repositorioCliente.save(cliente);
    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return cliente.codigosCuentas;
    }

  /*  @Override
    public Cuenta obtenerCuentaCliente(String documento, String numero) {

        final Cliente cliente = this.obtenerCliente(documento);

        if (!cliente.codigosCuentas.contains(numero))
            throw new RuntimeException("No existe cuenta [numero]: " + numero);
        return cuentaEncontrada;
    }*/

   /* @Override
    public void eliminarCuentaCliente(String documento, String cuenta) {
        final Cliente cliente = this.obtenerCliente(documento);
        boolean eliminado = cliente.cuentas.removeIf(c -> c.getNumero().equals(cuenta));
        if (Boolean.FALSE.equals(eliminado))
            throw new RuntimeException("No existe cuenta [numero]: " + cuenta);

        this.clientesMap.replace(cliente.documento, cliente);
    }

    @Override
    public void reemplazarCuentaCliente(String documento, Cuenta cuenta) {
        this.eliminarCuentaCliente(documento, cuenta.getNumero());
        this.agregarCuentaCliente(documento, cuenta);
    }

    @Override
    public void actualizarCuentaCliente(String documento, Cuenta cuenta) {
        final Cuenta cuentaExistente = this.obtenerCuentaCliente(documento, cuenta.getNumero());
        this.emparcharCuenta(cuentaExistente, cuenta);
        reemplazarCuentaCliente(documento, cuentaExistente);
    }*/

    public void emparcharCuenta(Cuenta cuentaExistente, Cuenta parche) {
        throw new UnsupportedOperationException("NO IMPLEMANTADO");
    }
}
