package mis.pruebas.demo.controlador;

import mis.pruebas.demo.modelos.Cliente;
import mis.pruebas.demo.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("${apirest.base.cliente}")
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    public static class ResumenCliente {
        public String documento;
        public String nombre;

        public static ResumenCliente of(Cliente x) {
            final ResumenCliente resumenCliente = new ResumenCliente();
            resumenCliente.documento = x.documento;
            resumenCliente.nombre = x.nombre;
            return resumenCliente;
        }
    }

    @GetMapping("/lista")
    public CollectionModel<EntityModel<ResumenCliente>> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {

        try {
            final var resumenesClientes =
                    this.servicioCliente.listaClientes(pagina - 1, cantidad)
                            .stream().map(x -> ResumenCliente.of(x)).collect(Collectors.toList());

            final var emClientes = resumenesClientes.stream()
                    .map(x -> obtenerMetadatosCliente(x)).collect(Collectors.toList());

            return CollectionModel.of(emClientes).add(
                    linkTo(methodOn(this.getClass()).obtenerClientes(1, 100))
                            .withSelfRel().withTitle("Todos los clientes")
            );

        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }


    }

    public EntityModel<ResumenCliente> obtenerMetadatosCliente(ResumenCliente x) {
        return EntityModel.of(x).add(
                linkTo(methodOn(this.getClass()).obtenerCliente(x.documento))
                        .withSelfRel().withTitle("Este cliente"),
                linkTo(methodOn(ControladorCuentaCliente.class).obtenerCuentascliente(x.documento))
                        .withRel("cuentas").withTitle("Cuentas del cliente")
        );
    }

    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {

        this.servicioCliente.crearCliente(cliente);

        final var location = linkTo(
                methodOn(ControladorCliente.class).obtenerCliente(cliente.documento)
        ).toUri();
        return ResponseEntity
                .ok()
                .location(location)
                .build();
    }

    @GetMapping("/{documento}")
    public EntityModel<Cliente> obtenerCliente(@PathVariable String documento) {

        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerCliente(documento)).withSelfRel();
            final Link cuentas = linkTo(methodOn(ControladorCuentaCliente.class).obtenerCuentascliente(documento)).withRel("cuentas").withTitle("Cuentas de este cliente");
            final Link clientes = linkTo(methodOn(this.getClass()).obtenerClientes(1, 100)).withRel("todosLosClientes").withTitle("Lista de todos los clientes");

            final var cliente = this.servicioCliente.obtenerCliente(documento);
            return EntityModel.of(cliente).add(self, cuentas, clientes);
        } catch (Exception ex) {
            System.err.println(String.format("obtenerUnCliente %s", documento));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/{documento}")
    public void reemplazarCliente(@PathVariable("documento") String nroDocumento, @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;

        try {
            this.servicioCliente.guardarCliente(cliente);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }


    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> emparcharUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> emparcharUnCliente("12345678", DATOS)
    @PatchMapping("/{documento}")
    public void emparacharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;
        this.servicioCliente.emparcharCliente(cliente);
    }

    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnCliente(@PathVariable("documento") String nroDocumento) {
        try {
            this.servicioCliente.borrarCliente(nroDocumento);
        } catch (Exception ex) {
        }
    }

}
