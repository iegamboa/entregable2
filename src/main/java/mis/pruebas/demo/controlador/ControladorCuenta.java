package mis.pruebas.demo.controlador;



import mis.pruebas.demo.modelos.Cuenta;
import mis.pruebas.demo.servicios.ServicioCliente;
import mis.pruebas.demo.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("${apirest.base.cuentas}")

public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;
    @Autowired
    ServicioCliente servicioCliente;

    // http://localhost:9000/api/v1/cuentas
    @GetMapping
    public List<Cuenta> obtenerCuentas() {
        return this.servicioCuenta.obtenerCuentas();
    }

    // http://localhost:9000/api/v1/cuentas + DATOS
    @PostMapping
    public ResponseEntity agregarCuenta(@RequestBody Cuenta cuenta) {

        this.servicioCuenta.insertarCuentaNueva(cuenta);

        final var location = linkTo(
                methodOn(ControladorCuenta.class).obtenerunaCuenta(cuenta.getNumero())
        ).toUri();
        return ResponseEntity
                .ok() // 200 OK
                .location(location) // Location: http://localhost:9000/api/v1/cuentas/901848745154
                .build(); // Mandar la resupesta.
    }

    // http://localhost:9000/api/v1/cuentas/12345678
    @GetMapping("/{numero}")
    public Cuenta obtenerunaCuenta(@PathVariable String numero) {
        return this.servicioCuenta.obtenerCuenta(numero);
    }

    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PutMapping("/{numero}")
    public void reemplazarunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta) {
        cuenta.setNumero(ctanumero);
        this.servicioCuenta.guardarCuenta(cuenta);

    }

    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PatchMapping("/{numero}")
    public void emparcharunaCuenta(@PathVariable("numero") String ctanumero,
                                   @RequestBody Cuenta cuenta) {
        cuenta.setNumero(ctanumero);
        this.servicioCuenta.emparcharCuenta(cuenta);
    }

    // DELETE http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try {
            this.servicioCuenta.borrarCuenta(numero);
        } catch (Exception x) {
        }
    }

}
