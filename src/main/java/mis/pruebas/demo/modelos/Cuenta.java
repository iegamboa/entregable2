package mis.pruebas.demo.modelos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
public class Cuenta {
    @Id
    private String numero;
    private String moneda;
    private String saldo;
    private String tipo;
    private String estado;
    private String oficina;
}
