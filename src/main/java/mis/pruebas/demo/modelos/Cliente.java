package mis.pruebas.demo.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@ToString
@Document
public class Cliente {


   /* @JsonIgnore
    public String id;*/
    @Id
    public String documento;
    public String nombre;
    public String edad;
    public String fechaNacimiento;
    public String direccion;
    public String telefono;
    public String correo;

    @JsonIgnore
    public List<String> codigosCuentas = new ArrayList<>();


}
